set dotenv-load
set export
HERE := env("HERE", invocation_directory())
FROM := env("FROM", "docker.io/library/buildpack-deps:stable")
DOCKERFILE := env("DOCKERFILE", HERE)
IMAGE := env("IMAGE", "registry.gitlab.com/ratio-case-os/docker/rust-ci")
TARGET := env("TARGET", "")
TAG := env("TAG", if TARGET == "" { "latest" } else { lowercase(TARGET) })
JUSTFILE := justfile()

# Show the recipe list.
default:
  @just --list --justfile {{JUSTFILE}}

# Build the OCI image.
build *args:
  podman build {{DOCKERFILE}} -t {{IMAGE}}:{{TAG}} --build-arg FROM={{FROM}} {{ if TARGET == "" {"--target BASE"} else { "--target " + TARGET } }} {{args}}

# Tag the current image with another tag.
tag tag:
  podman tag {{IMAGE}}:{{TAG}} {{IMAGE}}:{{tag}}

# Pull the FROM and built OCI image from the registry.
pull *args:
  podman pull {{FROM}} {{args}}
  podman pull {{IMAGE}}:{{TAG}} {{args}}

# Push the OCI image to the registry.
push *args:
  podman push {{IMAGE}}:{{TAG}} {{args}}

# Run the OCI image with the project directory mounted.
run args="" cmd="":
  podman run --userns=keep-id --rm -it -v {{HERE}}:/home/ratio/work:z {{args}} {{IMAGE}}:{{TAG}} {{cmd}}

# Run a recipe in the OCI image.
in *recipe:
  podman run --userns=keep-id --rm -it -v {{HERE}}:/home/ratio/work:z {{IMAGE}}:{{TAG}} just {{recipe}}

# Investigate the OCI image contents.
dive *args:
  dive --source podman {{IMAGE}}:{{TAG}} {{args}}
