# Rust CI image

Docker image used in Rust projects' CI/CD pipelines.

## Available tools

- [just](https://github.com/casey/just) as a proper alternative to faux Makefile abuse. A default [justfile](./justfile) is included in the home directory. Run `just` on in your shell to see the available commands.
- [cargo-deny](https://embarkstudios.github.io/cargo-deny/) for checking licenses and known issues.
- [cargo-edit](https://github.com/killercup/cargo-edit) for easy dependency upgrades.
- [cargo-tarpaulin](https://github.com/xd009642/tarpaulin) for code coverage.
- [flamegraph](https://github.com/flamegraph-rs/flamegraph) for performance measurements.
- [pnpm](https://pnpm.io) for web projects.
- [python3](https://packages.debian.org/stable/python3) for mixed package building.
- [pipx](https://pypa.github.io/pipx/) for isolated Python binary package installs.
- [poetry](https://python-poetry.org/) for Python dependency management.
- [trunk](https://trunkrs.dev/) for serving and building web projects.
- [twine](https://twine.readthedocs.io/en/stable/index.html) for package uploading to PyPI.
