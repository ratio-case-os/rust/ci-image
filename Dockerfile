ARG FROM=docker.io/library/buildpack-deps:stable
FROM ${FROM} as BASE
ARG USER=ratio

ENV DEBIAN_FRONTEND=noninteractive

# System packages
RUN apt-get update && apt-get install --no-install-recommends -qy \
    # web response parsing in pipelines
    jq \
    # minimal text editor
    nano \
    # for python mixed crates
    pipx \
    python-is-python3 \
    # for flamegraph
    linux-perf \
    # For serial communication
    libudev-dev \
    && rm -r /var/lib/apt/lists/*

# Setup Rust
ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:${PATH}
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs >> /rustup-init \
    && chmod +x /rustup-init \
    && /rustup-init -y --no-modify-path --profile default --default-host x86_64-unknown-linux-gnu \
    && rm /rustup-init \
    && chmod -R a+w ${RUSTUP_HOME} ${CARGO_HOME}

# Add targets and nightly toolchain with rustfmt installed.
RUN rustup toolchain install nightly --profile minimal --component rustfmt

# Setup and switch to a regular user
ENV HOME=/home/${USER}
ENV PATH=${HOME}/.local/bin:${PATH}
RUN useradd -m --uid 1000 --shell /bin/bash ${USER}
USER ${USER}
WORKDIR ${HOME}

# Add pnpm for web projects
ENV PNPM_HOME=${HOME}/.local/share/pnpm
ENV PATH=${PNPM_HOME}:${PATH}
RUN curl -fsSL https://get.pnpm.io/install.sh | SHELL=`which bash` bash -
RUN pnpm env use --global lts

# Add useful Python development tools for wrapped projects
RUN pipx ensurepath \
    && pipx install --pip-args=--no-cache poetry \
    && pipx install --pip-args=--no-cache twine \
    && pipx install --pip-args=--no-cache maturin

# Useful CLI utilities in Rust.
RUN cargo install --quiet --locked \
    cargo-deny \
    cargo-edit \
    cargo-expand \
    cargo-hack \
    cargo-outdated \
    cargo-tarpaulin \
    flamegraph \
    just \
    mdbook \
    trunk \
    wasm-pack \
    && rm -rf ${CARGO_HOME}/registry/ ${CARGO_HOME}/git/

USER ${USER}
VOLUME /work
WORKDIR /work

FROM BASE as CROSS

RUN rustup target add x86_64-pc-windows-msvc \
    && rustup target add x86_64-apple-darwin \
    && rustup target add aarch64-unknown-linux-gnu \
    && rustup target add aarch64-apple-darwin \
    && rustup target add aarch64-pc-windows-msvc \
    && rustup target add wasm32-unknown-unknown
